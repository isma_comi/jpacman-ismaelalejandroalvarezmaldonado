package cc3002.pclike.pacman;

/**
 * Class that contains the starter of the maps and displays the game
 * In this class we have the original map plus 2 more maps provided by ismael.
 * 
 * @author ismael
 *
 */
public class MapGenerator {
	
	/**
	 * the game builder, it has all we need to create the game
	 */
	private static GameBuilder game;
	
	/**
	 * this is the map given on the second homework
	 */
	public static void original(){
		game = new GameBuilder(10,10,0);
		game.addWall(3,3, 3, 5);
		game.addPacmanOn(1,1);
		game.buildAndOpen();
		game.addBlinkyOn(9,9);
		game.addInkyOn(1,10);
		game.addPinkyOn(10,1);
		game.addClydeOn(5,5);
	}
	
	/**
	 * this is the builder of my first map, it creates the game an add the stage and the actors
	 */
	public static void map1(){
		GameBuilder game=new GameBuilder(21,21,11);
		buildStage1(game);
		addActors1(game);
	}

	/**
	 * This adds the actors on the first map
	 * @param game is the game where the actors are going to be added
	 */
	protected static void addActors1(GameBuilder game) {
		game.addPacmanOn(11,10);
		game.buildAndOpen();
		game.addBlinkyOn(9,10);
		game.addInkyOn(9,9);
		game.addPinkyOn(9,11);
		game.addClydeOn(9,12);
	}

	/**
	 * this is the stage builder, it adds the non actor elements to the map
	 * @param game is the game where the elements are going to be added
	 */
	protected static void buildStage1(GameBuilder game) {
		game.addWall(6,3,6,4);
		game.addWall(4,4,5,4);
		game.addWall(7,4,8,4);
		game.addWall(5,8,5,13);
		game.addWall(6,3,6,4);
		game.addWall(4,17,8,17);
		game.addWall(6,18,6,19);
		
		game.addWall(9,8,10,8);
		game.addWall(9,13,10,13);
		game.addWall(10,8,10,13);
		
		game.addWall(14,4,19,4);
		game.addWall(17,4,17,6);
		game.addWall(14,6,15,6);
		game.addWall(15,7,15,8);
		game.addWall(16,8,19,8);
		game.addWall(19,6,19,7);
		
		game.addWall(14,10,14,18);
		game.addWall(19,10,19,18);
		game.addWall(16,10,18,10);
		game.addWall(16,14,18,14);
		game.addWall(15,12,17,12);
		game.addWall(15,16,17,16);
		game.addWall(17,17,17,18);
		
		game.addDotOn(5,3);
		game.addDotOn(5,18);
		game.addDotOn(16,5);
		game.addDotOn(16,7);
		game.addDotOn(18,8);
		game.addDotOn(18,7);
		game.addDotOn(15,11);
		game.addDotOn(15,13);
		game.addDotOn(15,15);
		game.addDotOn(18,11);
		game.addDotOn(18,13);
		game.addDotOn(18,13);
	}
	
	/**
	 * this is the second map created by me, 
	 * it generate the game and add the actors an the elements
	 */
	public static void map2(){
		GameBuilder game=new GameBuilder(21,21,10);
		buildStage2(game);
		addActors2(game);
	}

	/**
	 * this adds the actors to the game map
	 * @param game the game where the actors are going to be added
	 */
	protected static void addActors2(GameBuilder game) {
		game.addPacmanOn(5,10);
		game.buildAndOpen();
		game.addBlinkyOn(3,12);
		game.addInkyOn(3,8);
		game.addPinkyOn(3,9);
		game.addClydeOn(3,13);
	}

	/**
	 * this adds the non actors elements to the game map
	 * @param game is the game where the elements are going to be added
	 */
	protected static void buildStage2(GameBuilder game) {
		game.addWall(2,8,2,9);
		game.addWall(4,8, 4, 9);
		game.addWall(2,12, 2, 13);
		game.addWall(4,12,4,13);
		
		game.addWall(6,9,7,9);
		game.addWall(9,9,10,9);
		game.addWall(8,9,8,10);
		
		game.addWall(6,11,6,12);
		game.addWall(7,12,9,12);
		game.addWall(10,11,10,12);
		
		game.addWall(12,4,12,5);
		game.addWall(12,7,12,8);
		game.addWall(13,4,15,4);
		game.addWall(13,8,15,8);
		game.addWall(15,5,15,7);
		
		game.addWall(12,12,12,13);
		game.addWall(12,15,12,16);
		game.addWall(13,12,15,12);
		game.addWall(13,16,15,16);
		game.addWall(15,13,15,15);
		
		game.addDotOn(7,10);
		game.addDotOn(7,11);
		game.addDotOn(9,10);
		game.addDotOn(9,11);
		game.addDotOn(14,5);
		game.addDotOn(14,6);
		game.addDotOn(14,7);
		game.addDotOn(14,13);
		game.addDotOn(14,14);
		game.addDotOn(14,15);
	}
	
	/**
	 * this is the classic map of pacmap, it adds the actors and the non actors to the game
	 */
	public static void classic(){
		GameBuilder game=new GameBuilder(31,28,246);
		buildStageClassic(game);
		addActorsClassic(game);
	}
	
	/**
	 *  this adds the actors to the classic map on game
	 * @param g is the game where the actors are going to be added
	 */
	public static void addActorsClassic(GameBuilder g){
		g.addPacmanOn(24,14);
		g.buildAndOpen();
		g.addBlinkyOn(15,13);
		g.addInkyOn(15,14);
		g.addPinkyOn(15,15);
		g.addClydeOn(15,16);
	}
	
	/**
	 * this adds the non actors elements to the game map
	 * @param g is the game where the elements are going to be added
	 */
	public static void buildStageClassic(GameBuilder g){
		g.addWall(31,1,31,28);
		g.addWall(29,3,29,12);
		g.addWall(29,17,29,26);
		g.addWall(28,3,28,12);
		g.addWall(28,17,28,26);
		g.addWall(27,8,25,8);
		g.addWall(27,9,25,9);
		g.addWall(27,20,25,20);
		g.addWall(27,21,25,21);
		g.addWall(26,12,26,18);
		g.addWall(25,12,25,18);
		g.addWall(27,14,29,14);
		g.addWall(27,15,29,15);
		g.addWall(23,8,23,12);
		g.addWall(22,8,22,12);
		g.addWall(22,17,22,21);
		g.addWall(23,17,23,21);
		g.addWall(22,3,22,6);
		g.addWall(23,3,23,6);
		g.addWall(26,5,24,5);
		g.addWall(26,6,24,6);
		g.addWall(22,23,22,26);
		g.addWall(23,23,23,26);
		g.addWall(24,23,26,23);
		g.addWall(24,24,26,24);
		g.addWall(25,11,26,11);
		g.addWall(25,20,27,20);
		g.addWall(25,21,27,21);
		g.addWall(25,8,27,8);
		g.addWall(25,9,27,9);
		g.addWall(24,5,26,5);
		g.addWall(24,6,26,6);
		g.addWall(20,1,31,1);
		g.addWall(20,28,31,28);
		g.addWall(25,1,25,3);
		g.addWall(26,1,26,3);
		g.addWall(25,26,25,28);
		g.addWall(26,26,26,28);
		g.addWall(20,1,20,6);
		g.addWall(16,1,16,6);
		g.addWall(16,6,20,6);
		g.addWall(16,23,16,28);
		g.addWall(20,23,20,28);
		g.addWall(16,23,20,23);
		g.addWall(19,11,19,18);
		g.addWall(20,11,20,18);
		g.addWall(21,14,23,14);
		g.addWall(21,15,23,15);
		g.addWall(16,8,20,8);
		g.addWall(16,9,20,9);
		g.addWall(16,20,20,20);
		g.addWall(16,21,20,21);
		
		g.addWall(13,11,13,13);
		g.addWall(14,11,14,13);
		g.addWall(13,16,13,18);
		g.addWall(14,16,14,18);
		g.addWall(15,11,17,11);
		g.addWall(15,12,17,12);
		g.addWall(16,11,16,18);
		g.addWall(17,11,17,18);
		g.addWall(13,17,17,17);
		g.addWall(13,18,17,18);
		
		g.addWall(14,1,14,6);
		g.addWall(10,6,13,6);
		g.addWall(10,1,10,6);
		g.addWall(2,1,9,1);
		g.addWall(1,1,1,14);
		g.addWall(1,14,5,14);
		g.addWall(1,15,5,15);
		g.addWall(1,15,1,28);
		g.addWall(1,28,10,28);
		g.addWall(10,23,10,28);
		g.addWall(10,23,14,23);
		g.addWall(14,23,14,28);
		
		g.addWall(3,3,3,6);
		g.addWall(5,3,5,6);
		g.addWall(3,3,5,3);
		g.addWall(3,6,5,6);
		
		g.addWall(3,8,3,12);
		g.addWall(5,8,5,12);
		g.addWall(3,8,5,8);
		g.addWall(3,12,5,12);
		g.addWall(7,3,7,6);
		g.addWall(8,3,8,6);
		
		g.addWall(7,8,14,8);
		g.addWall(7,9,14,9);
		g.addWall(10,10,10,12);
		g.addWall(11,10,11,12);
		g.addWall(7,11,7,18);
		g.addWall(8,11,8,18);
		g.addWall(9,14,11,14);
		g.addWall(9,15,11,15);
		g.addWall(3,17,3,21);
		g.addWall(5,17,5,21);
		g.addWall(3,17,5,17);
		g.addWall(3,21,5,21);
		g.addWall(3,23,3,26);
		g.addWall(5,23,5,26);
		g.addWall(3,23,5,23);
		g.addWall(3,26,5,26);
		g.addWall(7,23,7,26);
		g.addWall(8,23,8,26);
		g.addWall(10,17,10,20);
		g.addWall(11,17,11,20);
		g.addWall(7,20,14,20);
		g.addWall(7,21,14,21);
		
		g.addDotOn(2,2);
		g.addDotOn(2,3);
		g.addDotOn(2,4);
		g.addDotOn(2,5);
		g.addDotOn(2,6);
		g.addDotOn(2,7);
		g.addDotOn(2,8);
		g.addDotOn(2,9);
		g.addDotOn(2,10);
		g.addDotOn(2,11);
		g.addDotOn(2,12);
		g.addDotOn(2,13);
		g.addDotOn(2,16);
		g.addDotOn(2,17);
		g.addDotOn(2,18);
		g.addDotOn(2,19);
		g.addDotOn(2,20);
		g.addDotOn(2,21);
		g.addDotOn(2,22);
		g.addDotOn(2,23);
		g.addDotOn(2,24);
		g.addDotOn(2,25);
		g.addDotOn(2,26);
		g.addDotOn(2,27);
		g.addDotOn(6,2);
		g.addDotOn(6,3);
		g.addDotOn(6,4);
		g.addDotOn(6,5);
		g.addDotOn(6,6);
		g.addDotOn(6,7);
		g.addDotOn(6,8);
		g.addDotOn(6,9);
		g.addDotOn(6,10);
		g.addDotOn(6,11);
		g.addDotOn(6,12);
		g.addDotOn(6,13);
		g.addDotOn(6,14);
		g.addDotOn(6,15);
		g.addDotOn(6,16);
		g.addDotOn(6,17);
		g.addDotOn(6,18);
		g.addDotOn(6,19);
		g.addDotOn(6,20);
		g.addDotOn(6,21);
		g.addDotOn(6,22);
		g.addDotOn(6,23);
		g.addDotOn(6,24);
		g.addDotOn(6,25);
		g.addDotOn(6,26);
		g.addDotOn(6,27);
		g.addDotOn(21,2);
		g.addDotOn(21,3);
		g.addDotOn(21,4);
		g.addDotOn(21,5);
		g.addDotOn(21,6);
		g.addDotOn(21,7);
		g.addDotOn(21,8);
		g.addDotOn(21,9);
		g.addDotOn(21,10);
		g.addDotOn(21,11);
		g.addDotOn(21,12);
		g.addDotOn(21,13);
		g.addDotOn(21,16);
		g.addDotOn(21,17);
		g.addDotOn(21,18);
		g.addDotOn(21,19);
		g.addDotOn(21,20);
		g.addDotOn(21,21);
		g.addDotOn(21,22);
		g.addDotOn(21,23);
		g.addDotOn(21,24);
		g.addDotOn(21,25);
		g.addDotOn(21,26);
		g.addDotOn(21,27);
		g.addDotOn(30,2);
		g.addDotOn(30,3);
		g.addDotOn(30,4);
		g.addDotOn(30,5);
		g.addDotOn(30,6);
		g.addDotOn(30,7);
		g.addDotOn(30,8);
		g.addDotOn(30,9);
		g.addDotOn(30,10);
		g.addDotOn(30,11);
		g.addDotOn(30,12);
		g.addDotOn(30,13);
		g.addDotOn(30,14);
		g.addDotOn(30,15);
		g.addDotOn(30,16);
		g.addDotOn(30,17);
		g.addDotOn(30,18);
		g.addDotOn(30,19);
		g.addDotOn(30,20);
		g.addDotOn(30,21);
		g.addDotOn(30,22);
		g.addDotOn(30,23);
		g.addDotOn(30,24);
		g.addDotOn(30,25);
		g.addDotOn(30,26);
		g.addDotOn(30,27);
		g.addDotOn(3,2);
		g.addDotOn(4,2);
		g.addDotOn(5,2);
		g.addDotOn(7,2);
		g.addDotOn(8,2);
		g.addDotOn(9,2);
		g.addDotOn(22,2);
		g.addDotOn(23,2);
		g.addDotOn(24,2);
		g.addDotOn(27,2);
		g.addDotOn(28,2);
		g.addDotOn(29,2);
		g.addDotOn(3,7);
		g.addDotOn(4,7);
		g.addDotOn(5,7);
		g.addDotOn(9,3);
		g.addDotOn(9,4);
		g.addDotOn(9,5);
		g.addDotOn(9,6);
		g.addDotOn(9,7);
		g.addDotOn(7,7);
		g.addDotOn(8,7);
		g.addDotOn(3,13);
		g.addDotOn(4,13);
		g.addDotOn(5,13);
		g.addDotOn(7,10);
		g.addDotOn(8,10);
		g.addDotOn(9,10);
		g.addDotOn(9,11);
		g.addDotOn(9,12);
		g.addDotOn(9,13);
		g.addDotOn(3,16);
		g.addDotOn(4,16);
		g.addDotOn(5,16);
		g.addDotOn(3,22);
		g.addDotOn(4,22);
		g.addDotOn(5,22);
		g.addDotOn(3,27);
		g.addDotOn(4,27);
		g.addDotOn(5,27);
		g.addDotOn(7,19);
		g.addDotOn(8,19);
		g.addDotOn(9,19);
		g.addDotOn(9,18);
		g.addDotOn(9,17);
		g.addDotOn(9,16);
		g.addDotOn(7,22);
		g.addDotOn(8,22);
		g.addDotOn(9,22);
		g.addDotOn(10,22);
		g.addDotOn(11,22);
		g.addDotOn(12,22);
		g.addDotOn(13,22);
		g.addDotOn(14,22);
		g.addDotOn(15,22);
		g.addDotOn(16,22);
		g.addDotOn(17,22);
		g.addDotOn(18,22);
		g.addDotOn(19,22);
		g.addDotOn(20,22);
		g.addDotOn(9,23);
		g.addDotOn(9,24);
		g.addDotOn(9,25);
		g.addDotOn(9,26);
		g.addDotOn(9,27);
		g.addDotOn(7,27);
		g.addDotOn(8,27);
		g.addDotOn(10,7);
		g.addDotOn(11,7);
		g.addDotOn(12,7);
		g.addDotOn(13,7);
		g.addDotOn(14,7);
		g.addDotOn(15,7);
		g.addDotOn(16,7);
		g.addDotOn(17,7);
		g.addDotOn(18,7);
		g.addDotOn(19,7);
		g.addDotOn(20,7);
		g.addDotOn(22,7);
		g.addDotOn(23,7);
		g.addDotOn(24,7);
		g.addDotOn(25,7);
		g.addDotOn(26,7);
		g.addDotOn(27,7);
		g.addDotOn(24,3);
		g.addDotOn(24,4);
		g.addDotOn(25,4);
		g.addDotOn(26,4);
		g.addDotOn(27,4);
		g.addDotOn(27,3);
		g.addDotOn(27,5);
		g.addDotOn(27,6);
		g.addDotOn(22,13);
		g.addDotOn(23,13);
		g.addDotOn(24,13);
		g.addDotOn(24,8);
		g.addDotOn(24,9);
		g.addDotOn(24,10);
		g.addDotOn(24,11);
		g.addDotOn(24,12);
		g.addDotOn(25,10);
		g.addDotOn(25,11);
		g.addDotOn(25,12);
		g.addDotOn(25,10);
		g.addDotOn(26,10);
		g.addDotOn(27,10);
		g.addDotOn(27,11);
		g.addDotOn(27,12);
		g.addDotOn(27,13);
		g.addDotOn(28,13);
		g.addDotOn(29,13);
		g.addDotOn(24,15);
		g.addDotOn(24,16);
		g.addDotOn(24,17);
		g.addDotOn(24,18);
		g.addDotOn(24,19);
		g.addDotOn(24,20);
		g.addDotOn(24,21);
		g.addDotOn(24,22);
		g.addDotOn(22,16);
		g.addDotOn(23,16);
		g.addDotOn(22,22);
		g.addDotOn(23,22);
		g.addDotOn(25,22);
		g.addDotOn(26,22);
		g.addDotOn(27,22);
		g.addDotOn(25,19);
		g.addDotOn(26,19);
		g.addDotOn(27,19);
		g.addDotOn(27,18);
		g.addDotOn(27,17);
		g.addDotOn(27,16);
		g.addDotOn(28,16);
		g.addDotOn(29,16);
		g.addDotOn(22,27);
		g.addDotOn(23,27);
		g.addDotOn(24,27);
		g.addDotOn(27,27);
		g.addDotOn(28,27);
		g.addDotOn(29,27);
		g.addDotOn(24,25);
		g.addDotOn(24,26);
		g.addDotOn(25,25);
		g.addDotOn(26,25);
		g.addDotOn(27,25);
		g.addDotOn(27,23);
		g.addDotOn(27,24);
		g.addDotOn(27,26);
		
		

	}

}
