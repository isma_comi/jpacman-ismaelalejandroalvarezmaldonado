package cc3002.pclike.pacman;



/**
 * Main class to start the game
 * 
 * @author ismael
 */
public class MainClass {
	/**
	 * main
	 * @param args
	 * @throws InterruptedException
	 */
	public static void main(String[] args) throws InterruptedException {
		//MapGenerator.original();
		//MapGenerator.map1();
		MapGenerator.map2();
		//MapGenerator.classic();
	}
	
}
