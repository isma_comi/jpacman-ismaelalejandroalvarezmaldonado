package cc3002.pclike.pacman;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import cc3002.pclike.elements.Blinky;
import cc3002.pclike.elements.Pacman;
import cc3002.pclike.map.PCBox;
import cc3002.pclike.map.PCMap;

public class PacmanTest {
	
	private GameBuilder game;
	private PCMap map;
	
	@Before
	public void setUp(){
		game = new GameBuilder(10,10,10);		
		game.buildAndOpen();
		map = game.getMap();
	}
		
	

	@Test
	public void testSceneryAndMovement() {
		
		game.addWall(3,3, 3, 5);
		game.addPacmanOn(1, 1);
		game.buildAndOpen();
		
		PCBox block = map.getBox(3, 3);
		PCBox emptybox = map.getBox(1, 2);
		Pacman pacman = game.getPacman();
		
		assertTrue(block.isBlock());
		assertFalse(emptybox.isBlock());
		
		pacman.walk();
		pacman.moveDown();
		
		pacman.moveLeft();
		
		pacman.moveRight();
		pacman.moveRight();
		
		pacman.moveDown();
		
		pacman.moveUp();
		
		pacman.moveUp();
		
	}

	
	@Test
	public void PacDotTest(){
		Pacman pacman = game.getPacman();
		game.addPacman();
		game.addDotOn(1,2);
		game.addDotOn(1,3);
		game.addDotOn(1,4);
		game.buildAndOpen();
		
		assertEquals(pacman.getEaten(),0);
		assertEquals(pacman.getTotalDots(), 10);
		
		pacman.walk();
		pacman.moveRight();
		pacman.walk();
		pacman.moveRight();
		pacman.walk();
		pacman.moveRight();
		pacman.walk();
		pacman.moveRight();
		pacman.walk();
		pacman.moveDown();
		assertEquals(pacman.getEaten(),3);		
	
	}
	

	@Test
	public void GhostTest(){
		game.addPacman();
		Pacman pacman = game.getPacman();
		game.buildAndOpen();
		pacman.moveTo(map.getBox(10,5), 10, 5);
		
		Blinky blinky = Blinky.getInstance();
		blinky.setPacman(pacman);
		pacman.addObserver(blinky);
		game.buildAndOpen();
		
		blinky.moveTo(map.getBox(3,6), 3, 6);
		blinky.chasePacman();
		assertEquals(blinky.getX(),4);
		assertEquals(blinky.getY(),6);
		blinky.moveToNextCell();
		blinky.moveToNextCell();
		blinky.moveToNextCell();
		blinky.moveToNextCell();
		
		
	
	}
	
	
	@Test
	public void DefaultMapTest(){
		MapGenerator.original();
		MapGenerator.map1();
		MapGenerator.map2();
		MapGenerator.classic();
		
		
		
	}


}

