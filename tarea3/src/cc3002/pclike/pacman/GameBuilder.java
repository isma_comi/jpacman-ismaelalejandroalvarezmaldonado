package cc3002.pclike.pacman;

import java.awt.BorderLayout;
import java.awt.GridLayout;

import javax.swing.JFrame;
import javax.swing.JPanel;

import cc3002.pclike.elements.*;
import cc3002.pclike.map.*;

/**
 * GameBuilder is the class that implements methods of the components of the game
 * So  it can generate the game as normal.
 * 
 * @author ismael
 */
public class GameBuilder {
	
	/**
	 * map reference.
	 */
	private PCMap map;
	/**
	 * max first index
	 */
	private int n;
	/**
	 * max second index
	 */
	private int m;
	/**
	 * reference of the singleton pacman
	 */
	private Pacman pacman;
	/**
	 * reference to the singleton blinky
	 */
	private Blinky blinky;
	/**
	 * refetence to the singleton inky
	 */
	private Inky inky;
	/**
	 * reference to the singleton pinky
	 */
	private Pinky pinky;
	/**
	 * reference to the singleton clyde
	 */
	private Clyde clyde;
	/**
	 * is the panel that has the info of the score and the lifes of the pacman
	 */
	private PCPanel scorePanel;
	
	/**
	 * initialises the game with the size of the grid and the numbers of dots
	 * 
	 * @param _n height
	 * @param _m width
	 * @param max number of dots in the map
	 */
	public GameBuilder(int _n,int _m,int max){
		n=_n;
		m=_m;
		map=new PCMap(n,m);
		pacman=Pacman.getInstance(max);
		inky=Inky.getInstance();
		pinky=Pinky.getInstance();
		blinky=Blinky.getInstance();
		clyde=Clyde.getInstance();
		scorePanel=new PCPanel(pacman); 
		pacman.addGhost(inky);
		pacman.addGhost(pinky);
		pacman.addGhost(blinky);
		pacman.addGhost(clyde);
	}
	
	/**
	 * Returns the PCMap of the game.
	 * 
	 * @return map the PCMap of the game.
	 */
	public PCMap getMap(){
		return map;
	}
	
	/**
	 * Returns the Pacman instance on the game
	 * 
	 * @return pacman the Pacman instance on the game
	 */
	public Pacman getPacman(){
		return pacman;
	}
	
	/**
	 * Sets a PCBlock at (x,y) 
	 * 
	 * @param x x coordinate
	 * @param y y coordinate
	 */
	public void addBlockOn(int x,int y){
		PCBlock block =new PCBlock();
		map.setBox(x,y,block);
	}
	
	/**
	 * Puts Pacman on the map at (1,1) 
	 */
	public void addPacman(){
		addPacmanOn(1,1);
	}
	
	/**
	 * Puts Pacman on the map at (x,y) 
	 * 
	 * @param x x coordinate
	 * @param y y coordinate
	 */
	public void addPacmanOn(int x,int y){
		addActorOn(pacman,x,y);
	}
	
	/**
	 * Puts an actor on the map at (x,y) 
	 * 
	 * @param actor actor to be added
	 * @param x x coordinate
	 * @param y y coordinate
	 */
	private void addActorOn(PCActor actor, int x,int y){
		actor.moveTo(map.getBox(x,y), x, y);
		actor.setDefoultPosition(map.getBox(x,y), x, y);
	}
	
	/**
	 * Puts Pinky on the mat at (x,y) 
	 * 
	 * @param x x coordinate
	 * @param y y coordinate
	 */
	public void addPinkyOn(int x, int y){
		addGhostOn(pinky, x, y);
	}
	
	/**
	 * Puts Clyde on the map at (x,y) 
	 * 
	 * @param x x coordinate
	 * @param y y coordinate
	 */
	public void addClydeOn(int x, int y){
		addGhostOn(clyde, x, y);
	}
	
	/**
	 * Puts Blinky on the map at (x,y) 
	 * 
	 * @param x x coordinate
	 * @param y y coordinate
	 */
	public void addBlinkyOn(int x, int y){
		addGhostOn(blinky, x, y);
	}
	
	/**
	 * Puts Inky on the map at (x,y) 
	 * 
	 * @param x x coordinate
	 * @param y y coordinate
	 */
	public void addInkyOn(int x, int y){
		addGhostOn(inky, x, y);
	}
	
	/**
	 *  Puts a ghost on the map at (x,y)
	 * @param ghost to be added
	 * @param x x coordinate
	 * @param y y coordinate
	 */
	private void addGhostOn(Ghost ghost, int x, int y){
		ghost.setPacman(pacman);
		pacman.addObserver(ghost);
		addActorOn(ghost,x,y);
		(new Thread(ghost)).start();
	}

	/**
	 * Puts a PCdot on the map at (x,y) 
	 * 
	 * @param x x coordinate
	 * @param y y coordinate
	 */
	public void addDotOn(int x,int y){
		PCdot pacdot = new PCdot(pacman);
		pacman.addObserver(pacdot);
		addActorOn(pacdot,x,y);
	}
	
	
	/**
	 * Puts a wall of blocks from (xo,yo) to (xf,yf)
	 * @param xo x coordinate of the starting
	 * @param yo y coordinate of the starting
	 * @param xf x coordinate of the ending
	 * @param yf y coordinate of the ending
	 */
	public void addWall(int xo, int yo, int xf,int yf){
		for(int x=xo;x<=xf;x++){
			for(int y=yo;y<= yf;y++){
				addBlockOn(x, y);
			}			
		}
	}
	
	/**
	 * displays the game and runs it all
	 */
	public void buildAndOpen(){
		map.loadBoxes();
		JPanel panel=new JPanel(new GridLayout(1,2));
		scorePanel.setJpanel(panel);
		pacman.addObserver(scorePanel);
		panel.add(scorePanel.getScoreLabel());
		panel.add(scorePanel.getLifesLabel());	
		JFrame frame=map.openInWindow();
		frame.setFocusable(true);
		frame.setResizable(false);
		frame.setSize(map.getPreferredSize());
		frame.addKeyListener(pacman);
		frame.add(panel,BorderLayout.SOUTH);
		map.updateUI();
	}
	
}
