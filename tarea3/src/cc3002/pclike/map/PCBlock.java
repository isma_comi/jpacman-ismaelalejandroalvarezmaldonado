package cc3002.pclike.map;

import java.awt.Color;


/**
 * PCBlock represents a wall in the game.
 * 
 * @author ismael
 */
public class PCBlock extends PCBox{

	/**
	 * serial version to jump eclipse warning
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Initalises a PCBlock.
	 */
	public PCBlock(){
		super();
		setOpaque(true);
		setBackground(Color.BLACK);
		revalidate();
	}

	/* (non-Javadoc)
	 * @see cc3002.pclike.map.PCBox#isBlock()
	 */
	@Override
	public boolean isBlock() {
		return true;
	}

}
