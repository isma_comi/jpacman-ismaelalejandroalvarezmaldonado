package cc3002.pclike.map;

import java.awt.BorderLayout;
import java.awt.GridLayout;

import javax.swing.*;


/**
 * PCMap is the class that implements and manipulates a grid of PCBoxes.
 *
 * @author ismael
 */
public class PCMap extends JPanel{
	
	/**
	 * serial version to jump eclipse warning
	 */
	private static final long serialVersionUID = 1L;
	
	
	/**
	 * the array of boxes of the map.
	 */
	private PCBox[][] box;
	/**
	 * first index of the height.
	 */
	private int n;
	/**
	 * second index of the width.
	 */
	private int m;
	
	/**
	 * Initialises the matrix of [N x M] PCBoxes.
	 * @param _n height
	 * @param _m width
	 */
	public PCMap(int _n,int _m){
		box = new PCBox[_n+2][_m+2];
		for(int i=0;i<_n+2;i++)
			for(int j=0;j<_m+2;j++)
				box[i][j]=new PCBlock();
		n = _n;
		m = _m;
		setLayout(new GridLayout(n,m));		
		fillMatrix();
		updateNeighbors();
	}
	
	/**
	 * Sets the PCbox in (i,j).
	 * 
	 * @param i position in x coordinate.
	 * @param j position in y coordinate.
	 * @param _box new box to be set on this position
	 */
	public void setBox(int i,int j,PCBox _box){
		box[i][j]=_box;
		updateNeighbors();
		updateUI();
	}
	/**
	 * getter of the box in (i,j) position. 
	 * 
	 * @param i x coordinate.
	 * @param j y coordinate.
	 * @return the box in(i,j) position in the map.
	 */
	public PCBox getBox(int i,int j){
		return box[i][j];
	}
	
	
	/**
	 * Makes the game map visible.
	 * 
	 * @return frame a JFrame window
	 */
	public JFrame openInWindow(){
		JFrame frame=new JFrame();
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setLayout(new BorderLayout());
		frame.add(this, BorderLayout.CENTER);
		frame.setVisible(true);
		updateUI();
		return frame;
	}
	
	/**
	 * Fills with blank boxes.
	 */
	private void fillMatrix(){
		for(int i=1; i<=n;i++){
			for(int j=1;j<=m;j++){
				box[i][j]=new PCEmptyBox();
			}
		}
	}
	
	/**
	 * Loads and ands the boxes.
	 */
	public void loadBoxes(){
		for(int i=1; i<=n;i++){
			for(int j=1;j<=m;j++){
				add(box[i][j]);
			}
		}
	}
	
	/**
	 * Loads the neighbors of the boxes.
	 */
	private void updateNeighbors(){
		for(int i=1; i<=n;i++){
			for(int j=1;j<=m;j++){
				box[i][j]
					.updateNeighbors(
							box[i-1][j],
							box[i+1][j],
							box[i][j-1],
							box[i][j+1]);
				box[i][j].updateBox();
			}
		}
	}
}
