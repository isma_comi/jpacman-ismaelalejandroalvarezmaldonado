package cc3002.pclike.map;

import java.awt.*;
import java.util.LinkedList;

import javax.swing.*;

import cc3002.pclike.elements.*;

/**
 * PCBox is the class for all types of boxes on JPacman game.
 * A PCBox represents the space that an element can use.  
 * A PCBox contains its neighbours.
 * 
 * @author ismailove
 */
@SuppressWarnings("serial")
public class PCBox extends JLabel{
	
	/**
	 * list of elements
	 */
	private LinkedList<PCElement> elements;
	
	/**
	 * top box of this box
	 */
	protected PCBox topBox;
	/**
	 * botom box of this box
	 */
	protected PCBox downBox;
	/**
	 * left box of this box
	 */
	protected PCBox leftBox;
	/**
	 * right box of this box
	 */
	protected PCBox rightBox;
	
	/**
	 * Initialises the stack of elements.
	 */
	public PCBox(){
		elements = new LinkedList<PCElement>();
		elements.add(new PCNullElement());
		setPreferredSize(new Dimension(24,24));
		setMaximumSize(new Dimension(24,24));
		setMinimumSize(new Dimension(24,24));
		setBorder(BorderFactory.createLineBorder(Color.BLACK));
		setOpaque(true);
	}
	
	/**
	 * Checks if this PCBox is a PCBlock instance. 
	 * @return false if not overwriten. 
	 */
	public boolean isBlock(){
		return false;
	}
	
	/**
	 * Moves one box up the PCelement on this box
	 * 
	 * @param element The PCelement to be moved to the upper box
	 */
	public void moveUp(PCElement element){}
	
	/**
	 * Moves one box down the PCelement on this box
	 * 
	 * @param element The PCelement to be moved to the lower box
	 */
	public void moveDown(PCElement element){}
	
	/**
	 * Moves one box to the left the PCelement on this box
	 * 
	 * @param element The PCelement to be moved to the left box
	 */
	public void moveLeft(PCElement element){}
	
	/**
	 * Moves one box to the right the PCelement on this box
	 * 
	 * @param element The PCelement to be moved to the right box
	 */
	public void moveRight(PCElement element){}
	
	/**
	 * Sets the new neighbours.
	 * 
	 * @param _top the top one
	 * @param _down the lower one
	 * @param _left the one on the left
	 * @param _right the one on the right
	 */
	public void updateNeighbors(PCBox _top, PCBox _down,PCBox _left, PCBox _right){
		topBox = _top;
		downBox = _down;
		leftBox = _left;
		rightBox = _right;
	}
	
	/**
	 * Adds a new element to the elements stack that. 
	 * 
	 * @param newElement a PCElement instance to be positioned on this box.
	 */
	public void add(PCElement newElement){
		elements.addFirst(newElement);
		updateBox();
	}
	
	/**
	 * Removes the indicated element from the elements stack that.
	 *   
	 * @param element a PCElement instance that to be removed in this box.
	 */
	public void remove(PCElement element){
		elements.remove(element);
		updateBox();
	}
	
	/**
	 * Makes visible the first element in the element stack in this PCBox.
	 * 
	 */
	public void updateBox(){
		setIcon(new ImageIcon(elements.get(0).fileImageName()));
		repaint();
	}
}
