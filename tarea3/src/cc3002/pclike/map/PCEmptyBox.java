package cc3002.pclike.map;


import cc3002.pclike.elements.*;

/**
 * PCEmptyBox is the class of boxes of the scenery where actors can be. 
 * Is an empty space in the game 
 * 
 * @author ismael
 */
public class PCEmptyBox extends PCBox{
	
	/**
	 * serial version to jump eclipse warning
	 */
	private static final long serialVersionUID = 1L;
	
	
	/* (non-Javadoc)
	 * @see cc3002.pclike.map.PCBox#moveUp(cc3002.pclike.elements.PCElement)
	 */
	@Override
	public void moveUp(PCElement element){
		element.moveTo(topBox,-1,0);
	}
	
	/* (non-Javadoc)
	 * @see cc3002.pclike.map.PCBox#moveDown(cc3002.pclike.elements.PCElement)
	 */
	@Override
	public void moveDown(PCElement element){
		element.moveTo(downBox,1,0);
	}
	/* (non-Javadoc)
	 * @see cc3002.pclike.map.PCBox#moveLeft(cc3002.pclike.elements.PCElement)
	 */
	@Override
	public void moveLeft(PCElement element){
		element.moveTo(leftBox,0,-1);
	}
	
	/* (non-Javadoc)
	 * @see cc3002.pclike.map.PCBox#moveRight(cc3002.pclike.elements.PCElement)
	 */
	@Override
	public void moveRight(PCElement element){
		element.moveTo(rightBox,0,1);
	}
	
}
