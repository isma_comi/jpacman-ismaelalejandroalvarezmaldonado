package cc3002.pclike.elements;

/**
 * Clyde is the class of the ghost Clyde on JPacman. Clyde moves random.
 *  
 * @author ismael
 *
 */
public class Clyde extends Ghost{
	
	/**
	 * unic instance of Clyde
	 */
	private static Clyde INSTANCE = new Clyde();
	
	/**
	 * Initialises a Clyde ghost with "yellow-ghost.png" image filename.
	 */
	private Clyde(){
		setImageFileName("yellow-ghost.png");
	}
	
	/**
	 * getter of the unic instance of Clyde
	 * 
	 * @return INSTANCE the unic instance of Clyde
	 */
	public static Clyde getInstance(){return INSTANCE;}

}

