package cc3002.pclike.elements;

import cc3002.pclike.map.PCBlock;
import cc3002.pclike.map.PCBox;


/**
 * PCActor is the abstract base class that represents main actors in the game.
 * Implements methods to manipulate the coordinates and the movement.
 * 
 * @author ismailove
 */
public abstract class PCActor extends PCElement{
	
	/**
	 * default box of the level
	 */
	protected PCBox defBox;
	/**
	 * default x of the level
	 */
	protected int defx;
	/**
	 * default y of the level
	 */
	protected int defy;
	
	
	/**
	 * reference to pacman.
	 */
	protected Pacman pacman;
	/**
	 * reference to the box the element is in.
	 */
	protected PCBox currentBox;
	/**
	 * position on the grid.
	 */
	protected int x, y;
	
	/**
	 * Sets a new actor in a PCBlock on (0,0).
	 */
	public PCActor(){
		currentBox=new PCBlock();
		x=0;
		y=0;
	}
	
	/**
	 * Getter of x.
	 * 
	 * @return x
	 */
	public int getX(){
		return x;
	}
	
	/**
	 * Getter of y.
	 * 
	 * @return y
	 */
	public int getY(){
		return y;
	}

	/**
	 * Moves the actor to (x+dx, y+dy).
	 * 
	 * @param dx variation in x.
	 * @param dy variation in y.
	 */
	public void addToCoords(int dx, int dy){
		x+= dx;
		y+= dy;
	}
	
	/* (non-Javadoc)
	 * @see cc3002.pclike.elements.PCElement#moveTo(cc3002.pclike.map.PCBox)
	 */
	public void moveTo(PCBox box, int x, int y){
		if(!box.isBlock()){
			currentBox.remove(this);
			currentBox=box;
			currentBox.add(this);
			this.addToCoords(x,y);
		}
	}
	
	/**
	 * it sets the default position of an actor, so it can go ther when the game starts again
	 * @param _box the initial box
	 * @param _x the initial x axis
	 * @param _y the initial y axis
	 */
	public void setDefoultPosition(PCBox _box, int _x, int _y){
		defBox = _box;
		defx = _x;
		defy = _y;
	}
	
	/**
	 * it transports the actor to the initial position
	 */
	public void moveToDefault(){
		currentBox.remove(this);
		currentBox=defBox;
		currentBox.add(this);
		this.addToCoords(defx,defy);
	}

	/**
	 * moves the actor to the upper box.
	 */
	public void moveUp() { currentBox.moveUp(this);}
	/**
	 * moves the actor to the belower box.
	 */
	public void moveDown() { currentBox.moveDown(this); }
	/**
	 * moves the actor to the box on the left.
	 */
	public void moveLeft() { currentBox.moveLeft(this); }
	/**
	 * moves the actor to the box on the right.
	 */
	public void moveRight() { currentBox.moveRight(this); }

	

}
