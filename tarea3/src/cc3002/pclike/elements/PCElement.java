package cc3002.pclike.elements;

import java.util.Observable;

import cc3002.pclike.map.PCBox;

/**
 * PCElement is the abstract class that represents an object of the game.
 * Implements methods to manipulate the image file name of the element.
 * 
 * @author ismael
 */
public abstract class PCElement extends Observable{
	/**
	 * string of the filename of tje image.
	 */
	protected String fileImageName;

	/**
	 * constructor of an object whith no image.
	 */
	public PCElement(){
		fileImageName="";
	}
	
	
	/**
	 * returns the filename of the image of the object.
	 * 
	 * @return fileImageName The filename of the image.
	 */
	public String fileImageName(){
		return fileImageName;
	}
	
	/**
	 * Set of the filename of the image.
	 * @param name the new direction of the image.
	 */
	public void setImageFileName(String name){
		fileImageName=name;
	}
	
	/**
	 * Moves the element in (x+dx,y+dy) to the box box.
	 * 
	 * @param box The PCBox where the element is going to be moved.
	 * @param dx distance on x to be moved. 
	 * @param dy distance on y to be moved. 
	 */
	public void moveTo(PCBox box, int dx, int dy){
		
	}

}
