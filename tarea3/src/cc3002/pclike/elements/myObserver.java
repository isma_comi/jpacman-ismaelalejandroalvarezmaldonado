package cc3002.pclike.elements;

/**
 * this is the interface of an observer, like seen on the observer pattern
 * this observer performs an action when the observable notifies
 * @author ismael
 *
 */
public interface myObserver {
	
	/**
	 * this is the action that every observer has to perform
	 */
	void performAction();

}
