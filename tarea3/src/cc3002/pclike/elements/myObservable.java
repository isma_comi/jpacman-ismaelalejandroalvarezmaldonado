package cc3002.pclike.elements;

/**
 * this is the interface of my observables, is used to notifies the ghosts when pacman
 * dies, so they can go to the staring position
 * @author ismael
 *
 */
public interface myObservable {
	
	/**
	 * this is the action of notifying the ghosts that they have to perform their action
	 */
	void notifyGhosts();

}

