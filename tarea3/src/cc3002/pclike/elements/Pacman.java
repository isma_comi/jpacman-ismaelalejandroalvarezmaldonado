package cc3002.pclike.elements;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.ArrayList;

import cc3002.pclike.map.PCBox;

/**
 * Pacman is the class that represents the singleton pacman in JPacman.
 * To win, Pacman musts eat every pac-dot on the map and avoid the ghosts.
 *
 * @author ismael
 */
public class Pacman extends PCActor implements KeyListener, myObservable{
	
	
	/**
	 * the array of ghosts to be notified by the pacman when it dies.
	 */
	private ArrayList<myObserver> ghosts = new ArrayList<myObserver>();
	
	/**
	 * it adds a ghost to observe pacman
	 * @param g the ghost to be added to the list of the observers
	 */
	public void addGhost(Ghost g){
		ghosts.add(g);
	}
	
	
	/**
	 * pacman lifes
	 */
	private int lives;
	/**
	 * unic instance of pacman
	 */
	private static Pacman INSTANCE = new Pacman();
	/**
	 * eaten dots.
	 */
	private int eaten;
	/**
	 * max number of dots to win.
	 */
	private static int maxdots;
	/**
	 * score of pacman
	 */
	private PCScore score = new PCScore();
	/**
	 * steps done by pacman
	 */
	private int steps;
	
	/**
	 * private constructor for the singleton
	 */
	private Pacman() {
		eaten = 0;
		lives = 3;
		steps = 0;
		setImageFileName("pacman.png");
	}
	
	/**
	 * getter of the lives of pacman
	 * @return lives the lives pacman has left
	 */
	public int getLives(){
		return lives;
	}
	
	/**
	 * geter of the singleton instance
	 * 
	 * @return INSTANCE is the only instance of the pacman
	 */
	public static Pacman getInstance(int _maxdots) {
		maxdots = _maxdots;
        return INSTANCE;
    }
	
	/**
	 * Asks if pacman is death, if lives had decreced to 0.
	 * @return boolean true if lives is 0 false if not.
	 */
	public boolean isDeath(){
		return lives==0;
	}
	
	/**
	 * it asks the score how many points i have
	 * @return score.getPoints() is the total score of the pacman
	 */
	public int getScore(){return score.getPoints();}
	
	/**
	 * this perform the action of dying once, so it substracts a life to the pacman
	 * it modifies the score, and notifies the gosts that pacmans has died.
	 */
	public void diedOnce(){
		score.pacmanDies();
		this.moveToDefault();
		notifyGhosts();
		lives = lives - 1;
	}
	
	/**
	 * Returns the number of dots on the game map at the begining.
	 * 
	 * @return maxdots is the number of dots on the map at the begining . 
	 */
	public int getTotalDots(){
		return maxdots;
	}
	
	/**
	 * Returns the number of dots Pacman has eaten.
	 * 
	 * @return eaten is the number of dots Pacman has eaten.
	 */
	public int getEaten(){
		return eaten;
	}
	
	/**
	 * Adds one more pac-dot to the eaten pac-dots counter.
	 */
	public void eatDot(){
		eaten+=1;
		score.addScoreSteps(steps);
		if(eaten==maxdots){score.addScoreWin(lives);}
	}
	
	/* (non-Javadoc)
	 * @see cc3002.pclike.elements.PCActor#moveTo(cc3002.pclike.map.PCBox, int, int)
	 */
	@Override
	public void moveTo(PCBox box, int x, int y){
		super.moveTo(box, x, y);
		setChanged();
		notifyObservers();
	}
	
	/* (non-Javadoc)
	 * @see java.awt.event.KeyListener#keyPressed(java.awt.event.KeyEvent)
	 */
	@Override
	public void keyPressed(KeyEvent event) {
		walk();
		switch(event.getKeyCode()){
			case KeyEvent.VK_UP :
				moveUp();
				break;
			case KeyEvent.VK_DOWN:
				moveDown();
				break;
			case KeyEvent.VK_LEFT:
				moveLeft();
				break;
			case KeyEvent.VK_RIGHT: 
				moveRight();
				break;
			}
	}

	/**
	 * this is the idea of walking
	 */
	public void walk() {
		steps = steps +1;
	}
	/* (non-Javadoc)
	 * @see java.awt.event.KeyListener#keyReleased(java.awt.event.KeyEvent)
	 */
	@Override
	public void keyReleased(KeyEvent event) {
		
	}

	/* (non-Javadoc)
	 * @see java.awt.event.KeyListener#keyTyped(java.awt.event.KeyEvent)
	 */
	@Override
	public void keyTyped(KeyEvent event) {
	
	}

	/* (non-Javadoc)
	 * @see cc3002.pclike.elements.myObservable#notifyGhosts()
	 */
	@Override
	public void notifyGhosts() {
		for(myObserver g: ghosts){
			g.performAction();
		}
		
	}
}
