package cc3002.pclike.elements;

/**
 * Pinky is the class of the ghost Pinky on JPacman. Pinky moves random.
 * 
 * @author ismael
 *
 */
public class Pinky extends Ghost{
	
	/**
	 * Unic instance of pinky
	 */
	private static Pinky INSTANCE = new Pinky();
	
	/**
	 * Initialises a Pinky ghost with "pink-ghost.png" image filename.
	 */
	private Pinky(){
		setImageFileName("pink-ghost.png");
	}
	
	/**
	 * getter of the unic instance of pinky
	 * 
	 * @return INSTANCE is the unic instance of Pinky
	 */
	public static Pinky getInstance(){return INSTANCE;}

}
