package cc3002.pclike.elements;

import java.util.Observable;
import java.util.Observer;

import javax.swing.JOptionPane;

import cc3002.pclike.map.PCBox;


/**
 * Ghost is the class that represents a ghost on JPacman.
 * A Ghost instance is a <code>Pacman</code> instance observer.
 * 
 * @author ismailove
 */
public class Ghost extends PCActor implements Runnable, Observer, myObserver{
	
	
	/**
	 * this is the reference to the pacman
	 */
	protected Pacman pacman;
	/**
	 * this is the idea of the ghost being active thread
	 */
	protected boolean active; 
	/**
	 * this is the normal speed of a ghost
	 */
	protected int speed; // milliseconds
	
	/**
	 * Sets a new active ghost that moves 1 cell per second. 
	 */
	public Ghost(){
		active = true;
		speed = 1000;
		
	}
	
	/**
	 * Sets the <code>Pacman</code> instance on the game as a field of this class.
	 * 
	 * @param _pacman Pacman actor on the game.
	 */
	public void setPacman(Pacman _pacman){
		pacman = _pacman;
	}
	
	/**
	 * performs super.moveTo and checks if Pacman and the ghost are in the same 
	 * position on the game grid.
	 */
	public void moveTo(PCBox box, int x, int y){
		super.moveTo(box, x, y);
		checkPosition();
	}
	
	/**
	 * Moves the ghost randomly.
	 */
	public void moveToNextCell(){
		checkPosition();
		double random=Math.random();
		if(random<0.25) moveDown();
		else if(random <0.5)moveLeft();
		else if(random <0.75) moveRight();
		else moveUp();
	}
	
	/**
	 * Stops the ghost activity.
	 */
	public void disable(){
		active=false;
	}
	
	
	/* (non-Javadoc)
	 * @see java.lang.Runnable#run()
	 */
	@Override
	public void run(){
		while(active){
			try{Thread.sleep(speed);} catch(Exception e){}
			this.moveToNextCell();
		}
	}
	
	/* (non-Javadoc)
	 * @see java.util.Observer#update(java.util.Observable, java.lang.Object)
	 */
	@Override
	public void update(Observable obs, Object args) {
		checkPosition();
	}
	
	/**
	 * Checks if the ghost and Pacman are on the same position on the game grid.
	 * If they are, we show up the mesage "Game Over" then the game will finish.
	 */
	public void checkPosition(){
		if(pacman.currentBox == currentBox){
			if(pacman.isDeath()){
				JOptionPane.showMessageDialog(null,"Game Over");
				System.exit(0);
			}
			else{
				pacman.diedOnce();
				}
		}
	}
	
	/**
	 * Returns the number of dots Pacman has eaten.
	 * 
	 * @return the number of dots Pacman  has eaten.
	 */
	public int checkDots(){
		return pacman.getEaten();
	}

	/* (non-Javadoc)
	 * @see cc3002.pclike.elements.myObserver#performAction()
	 */
	@Override
	public void performAction() {
		this.moveToDefault();
		
	}

}
