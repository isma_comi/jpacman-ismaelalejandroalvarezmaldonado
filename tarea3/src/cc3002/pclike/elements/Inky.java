package cc3002.pclike.elements;

/**
 * Inky is the class that the ghost Inky JPacman. 
 * It moves at speed of 1 box every 1200 milliseconds. 
 * He randomly chooses whether to wander through the map like Clyde and Pinky 
 * or to run from Pacman.
 * 
 * @author ismael
 *
 */
public class Inky extends Ghost {
	
	/**
	 * unic instance of inky
	 */
	private static Inky INSTANCE = new Inky();
	
	/**
	 * Initialises an Inky ghost with "blue-ghost.png" image filename 
	 * and speed of 1 box per 1200 milliseconds.
	 */
	private Inky(){
		speed = 1200;
		setImageFileName("blue-ghost.png");
	}
	
	/**
	 * getter of the unic instance of Inky
	 * 
	 * @return INSTANCE the unic instance of Inky
	 */
	public static Inky getInstance(){return INSTANCE;}
	
	/**
	 * Moves the ghost towards the nearest cell that greatest the most the distance
	 * between itself and the Pacman instance on the game.
	 */
	public void runFromPacman(){
		int dx = Math.abs(x - pacman.x);
		int dy = Math.abs(y - pacman.y);
		if (dx > dy){
			if((x-pacman.x)>0) moveDown();
			else moveUp();
		}
		else{
			if((y-pacman.y)>0) moveRight();
			else moveLeft();
		}
	}
	
	/* (non-Javadoc)
	 * @see cc3002.pclike.elements.Ghost#run()
	 */
	@Override
	public void run(){
		while(active){
			try{Thread.sleep(speed);} catch(Exception e){}
			double random = Math.random();
			if(random>0.5) this.runFromPacman();
			else this.moveToNextCell();
		}
		
	}
	
	

}