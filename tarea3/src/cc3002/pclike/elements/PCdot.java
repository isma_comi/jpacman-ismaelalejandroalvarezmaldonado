package cc3002.pclike.elements;

import java.util.Observable;
import java.util.Observer;

import javax.swing.JOptionPane;

import cc3002.pclike.map.PCEmptyBox;

/**
 * PCDot is the class that represents the dots JPacman eats.
 * It disappears of the game when Pacman passes through it.
 * Pacman must eat all the pac-dots to win the game.
 * 
 * @author ismael
 */
public class PCdot extends PCActor implements Observer{
	
	
	/**
	 * Is the reference to pacman in the game.
	 */
	private Pacman pacman;
	
	/**
	 * Initialises the PCDot with "dot.png" image filename.
	 * 
	 * @param _pacman The Pacman instance on the game.
	 */
	public PCdot(Pacman _pacman){
		pacman = _pacman;
		setImageFileName("dot.png");
	}
	
	/* (non-Javadoc)
	 * @see java.util.Observer#update(java.util.Observable, java.lang.Object)
	 */
	@Override
	public void update(Observable obs, Object args) {
		checkPosition();
	}
	
	/**
	 * Checks if Pacman is in the same position of this dot. 
	 * Then the dot is removed from the map.
	 * And 1 dot is added to the Pacman's eaten-dots counter. 
	 * Also checks if there are more pac-dots left on the map.
	 */
	public void checkPosition() {
		if(pacman.currentBox == currentBox){
			pacman.eatDot();
			currentBox.remove(this);
			currentBox= new PCEmptyBox();
			this.noDotsLeft();
			}
		
	}
	
	/**
	 * Checks if there are no more pac-dots left on the game map.
	 * If that's the case, a Message Dialog will show up with the sentence "Congratulations! You won!" written on it and the game will finish.
	 */
	public void noDotsLeft() {
		if(pacman.getEaten() == pacman.getTotalDots()){
			JOptionPane.showMessageDialog(null,"Congratulations! You Won! -score: " + pacman.getScore());
			System.exit(0);
			
		}
		
	}	
}
