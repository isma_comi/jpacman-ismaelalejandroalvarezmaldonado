package cc3002.pclike.elements;

import java.util.Observable;

/**
 * Blinky is the class of the ghost Blinky on JPacman. it moves 2 boxes per second and constantly chases Pacman through the map.
 * When Pacman has eaten 8 pac-dots, Blinky's speed boosts up to 1 box per 350 milliseconds.
 * 
 * @author ismael
 */
public class Blinky extends Ghost{

	/**
	 * unic instance of Blinky
	 */
	private static Blinky INSTANCE = new Blinky();
	
	/**
	 * Initialises Blinky, with the image filename "red-ghost_Right.png" and speed of 1 box per 500 milliseconds.
	 */
	private Blinky(){
		speed = 500;
		setImageFileName("red-ghost.png");
	}
	
	/**
	 * getter of the unic instance of Blinky
	 * 
	 * @return INSTANCE the unic instance of Blinky
	 */
	public static Blinky getInstance(){return INSTANCE;}
	
	/**
	 * Moves the ghost towards the nearest cell that shortens the most the distance
	 * between itself and the Pacman instance on the game.
	 */
	public void chasePacman(){
		int dx = Math.abs(x - pacman.x);
		int dy = Math.abs(y - pacman.y);
		if (dx > dy){
			if((x-pacman.x)>0) moveUp();
			else moveDown();
		}
		else{
			if((y-pacman.y)>0) moveLeft();
			else moveRight();
		}
	}
	
	/* (non-Javadoc)
	 * @see cc3002.pclike.elements.Ghost#run()
	 */
	@Override
	public void run(){
		while(active){
			try{Thread.sleep(speed);} catch(Exception e){}
			this.chasePacman();
		}
	}
	
	/* (non-Javadoc)
	 * @see cc3002.pclike.elements.Ghost#update(java.util.Observable, java.lang.Object)
	 */
	@Override
	public void update(Observable obs, Object args) {
		super.update(obs,args);
		if(pacman.getEaten() == 8) speed = 350;
	}

}
