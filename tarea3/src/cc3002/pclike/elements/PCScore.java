package cc3002.pclike.elements;

/**
 * This is the class that encapsulates the idea of score on pacman,
 * it manage how to add points and substracts points
 * 
 * 
 * @author ismael
 *
 */
public class PCScore {
	
	/**
	 * this is the int that represents the score
	 */
	private int points;
	
	/**
	 * this is the constructor, it sets the score to 0
	 */
	public PCScore(){
		points = 0;
	}
	
	/**
	 * it adds the score from eating a point in i steps
	 * @param i is the number of steps
	 */
	public void addScoreSteps(int i){
		points = points + 1000/i;
	}
	
	/**
	 * it adds the score from winning the game
	 * @param l is the number of lifes left on pacman
	 */
	public void addScoreWin(int l){
		points = points + 1000*l;
	}
	
	/**
	 * it reset the score when pacman dies
	 */
	public void pacmanDies(){
		points = 0;
	}
	
	/**
	 * getter of the poins
	 * @return points is the integer that has the total of points
	 */
	public int getPoints(){
		return points;
	}

}
