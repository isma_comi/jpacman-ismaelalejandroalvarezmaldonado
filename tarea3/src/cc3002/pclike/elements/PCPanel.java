package cc3002.pclike.elements;

import java.util.Observable;
import java.util.Observer;

import javax.swing.JLabel;
import javax.swing.JPanel;

/**
 * this is the class that encapsulates the idea of the info panel of the game pacman
 * 
 * @author ismael
 *
 */
public class PCPanel implements Observer{

	/**
	 * reference to pacman so it can update the score and the lifes
	 */
	private Pacman pacman;
	/**
	 * lifes are the lifes pacman has left
	 */
	private int lifes;
	/**
	 * score is the score pacman has
	 */
	private int score;
	/**
	 * is the label that contains the string of the score
	 */
	private JLabel scoreLabel;
	/**
	 * is the label that contains the string of the lifes
	 */
	private JLabel lifesLabel;
	/**
	 * is the panel to be upgraded
	 */
	private JPanel jpanel;
	
	/**
	 * constructor of the class so we can have the information of the game setion
	 * @param p is the singleton of pacman
	 */
	public PCPanel(Pacman p){
		pacman = p;
		lifes = pacman.getLives();
		score = pacman.getScore();
		lifesLabel = new JLabel(this.lifesString());
		scoreLabel = new JLabel(this.scoreString());
	}
	
	/**
	 * setter of the panel so we can upgrade it
	 * @param _panel
	 */
	public void setJpanel(JPanel _panel){
		jpanel = _panel;
	}
	
	/**
	 * generates a string whit the score of pacman
	 * @return score is the score of the pacman
	 */
	public String scoreString(){
		return "score: " + score;
	}
	
	/**
	 * generates a string whith the lifes pacman has left
	 * @return lifes is the lifes pacman has left
	 */
	public String lifesString(){
		return "lifes: " + lifes;
	}
	
	/**
	 * update reloads the information in the panel asking pacman its score and lifes left
	 * it removes the old labels and replace them whith new ones whit the new info
	 */
	@Override
	public void update(Observable o, Object arg) {
		lifes = pacman.getLives();
		score = pacman.getScore();
		jpanel.remove(lifesLabel);
		jpanel.remove(scoreLabel);
		lifesLabel = new JLabel(this.lifesString());
		scoreLabel = new JLabel(this.scoreString());
		jpanel.add(scoreLabel);
		jpanel.add(lifesLabel);
		jpanel.repaint();
	}
	
	/**
	 * it returns the life label of the panel
	 * @return lifeLabel is the label of the lifes
	 */
	public JLabel getLifesLabel(){
		return lifesLabel;
	}
	
	/**
	 * it returns the score label of the panel
	 * @return scoreLabel is the label of the score
	 */
	public JLabel getScoreLabel(){
		return scoreLabel;
	}

}
