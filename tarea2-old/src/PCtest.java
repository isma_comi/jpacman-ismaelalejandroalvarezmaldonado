import static org.junit.Assert.*;

import java.awt.event.KeyEvent;
import java.util.Observable;

import javax.swing.JFrame;


import org.junit.Test;
import cc3002.pclike.pacman.*;
import cc3002.pclike.elements.*;
import cc3002.pclike.map.*;

public class PCtest {

	@Test
	public void testdireccion(){
		direccion a=new abajo();
		assertTrue(a instanceof abajo);
		direccion b=new arriba();
		assertTrue(b instanceof arriba);
		direccion c=new derecha();
		assertTrue(c instanceof derecha);
		direccion d=new izquerda();
		assertTrue(d instanceof izquerda);
	}
	
	@Test
	public void testapple(){
		Apple a=new Apple();
		assertTrue(a instanceof Apple);
	}
	
	@SuppressWarnings("deprecation")
	@Test
	public void tesghostandpc(){
		Pacman pc=Pacman.getInstance();
		Pacman pc2=Pacman.getInstance();
		assertTrue(pc==pc2);
		assertTrue(pc.getComido()==0);
		pc.come();
		assertTrue(pc.getComido()==1);
		pc.come();
		pc.come();
		pc.come();
		pc.come();
		pc.come();
		pc.come();
		bGhost blinky=new bGhost(pc);
		try{blinky.moveToNextCell();}
		catch (ClassCastException e) {assertTrue(true);}
		assertTrue(blinky.getSpeed()==500);
		pc.come();
		blinky.update(pc,new Object());
		assertTrue(blinky.getSpeed()==350);
		
		iGhost inky=new iGhost(pc);
		try{inky.moveToNextCell();}
		catch (ClassCastException e) {assertTrue(true);}
		
		pcGhost p=new pcGhost(pc);
		p.disable();
		p.run();
		p.moveTo(new PCBox());
		p.moveToNextCell();
		assertTrue(true);
		
		try{pc.keyTyped(new KeyEvent(null, 0, 0, 0, 0));}
		catch (IllegalArgumentException e) {assertTrue(true);}
		pc.keyReleased(null);
		try{pc.keyPressed(new KeyEvent(null, 0, 0, 0, 0));}
		catch (IllegalArgumentException e) {assertTrue(true);}
		try{pc.moveTo(new PCBox());}
		catch (NullPointerException e) {assertTrue(true);}
		pc.setallstart(0);
		assertTrue(pc.getMax()==0);
		
	}
	
	@Test
	public void testelementactornull(){
		Pacman pc=Pacman.getInstance();
		PCActor actor=new pcGhost(pc);
		
		actor.moveTo(new PCBox());
		actor.moveUp();
		actor.moveDown();
		actor.moveLeft();
		actor.moveRight();
		assertTrue(true);
		
		PCElement element=new pcGhost(pc);
		element.setImageFileName("asd");
		assertTrue(element.fileImageName().equals("asd"));
		element.moveTo(new PCBox());
		assertTrue(true);
		
		PCNullElement ne=new PCNullElement();
		assertTrue(ne instanceof PCNullElement);
	}
	
	@Test
	public void testblockboxmapnullgame(){
		PCBlock blk=new PCBlock();
		assertTrue(blk.isBlock());
		PCBox blk2=new PCBlock();
		assertTrue(blk2.isBlock());
		PCBox bx=new PCBox();
		assertFalse(bx.isBlock());
		Pacman pc=Pacman.getInstance();
		PCElement element=new pcGhost(pc);
		bx.moveUp(element);
		bx.moveDown(element);
		bx.moveLeft(element);
		bx.moveRight(element);
		bx.updateNeighbors(bx,bx,bx,bx);
		bx.add(element);
		bx.updateBox();
		bx.remove(element);
		assertTrue(true);
		
		PCEmptyBox eb=new PCEmptyBox(1,2);
		assertTrue(eb.geti()==1);
		assertTrue(eb.getj()==2);
		try{eb.moveUp(element);}
		catch (NullPointerException e) {assertTrue(true);}
		try{eb.moveDown(element);}
		catch (NullPointerException e) {assertTrue(true);}
		try{eb.moveLeft(element);}
		catch (NullPointerException e) {assertTrue(true);}
		try{eb.moveRight(element);}
		catch (NullPointerException e) {assertTrue(true);}
		
		PCMap map=new PCMap(10,10);
		map.setBox(2, 2, bx);
		assertTrue(map.getBox(2,2)==bx);
		assertTrue(map.openInWindow() instanceof JFrame);
		map.loadBoxes();
		
		GameBuilder gb=new GameBuilder(10, 10, 10);
		gb.addBlockOn(3,3);
		gb.addPacman();
		gb.addDotOn(5,5);
		gb.addBlinkyOn(10,10,GameBuilder.BLINKY);
		gb.addInkyOn(1,10,GameBuilder.INKY);
		gb.addPinkyOn(10,1,GameBuilder.PINKY);
		gb.addClydeOn(5,5,GameBuilder.CLYDE);
		gb.addWall(3,3, 3, 5);
		assertTrue(true);
	}

}
