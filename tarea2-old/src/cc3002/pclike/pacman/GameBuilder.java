package cc3002.pclike.pacman;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;
import javax.swing.Timer;

import cc3002.pclike.elements.*;
import cc3002.pclike.map.*;

/**
 * @author ismailove
 * clace que genera el tablero
 */
public class GameBuilder {
	
	/**
	 * referencia del mapa
	 */
	private PCMap map;
	/**
	 * maximo primer indice
	 */
	private int n;
	/**
	 * maximo segundo indice
	 */
	private int m;
	/**
	 * referencia a la unica instancia del pacman
	 */
	private Pacman pacman;
	/**
	 * direccion de la imagen de blinky
	 */
	public static final String BLINKY="red-ghost.png";
	/**
	 * direccion de la imagen de inky
	 */
	public static final String INKY="blue-ghost.png";
	/**
	 * direccion de la imagen de pinky
	 */
	public static final String PINKY="pink-ghost.png";
	/**
	 * direccion de la imagen de clyde
	 */
	public static final String CLYDE="yellow-ghost.png";
	/**
	 * direccion de la imagen de dot
	 */
	public static final String DOT="dot.png";
	/**
	 * direccion de la imagen de pacman
	 */
	public static final String PACMAN="pacman.png";
	
	
	/**
	 * crea el juego con el tamaño maximo y cuantos dots hay
	 * @param _n
	 * @param _m
	 * @param max cuantos dots hay
	 */
	public GameBuilder(int _n,int _m,int max){
		n=_n;
		m=_m;
		map=new PCMap(n,m);
		pacman=Pacman.getInstance();
		pacman.setallstart(max);
	}
	
	
	/**
	 * agrega un bloque en la posicion x,y
	 * @param x
	 * @param y
	 */
	public void addBlockOn(int x,int y){
		PCBlock block =new PCBlock();
		map.setBox(x,y,block);
	}
	
	/**
	 * agrega a pacman en el default 1,1
	 */
	public void addPacman(){
		addPacmanOn(1,1);
	}
	
	/**
	 * agrega a pacman en x,y
	 * @param x
	 * @param y
	 */
	public void addPacmanOn(int x,int y){
		addPacmanOn(x,y,PACMAN);
	}
	
	/**
	 * agrega un dot en x,y
	 * @param x
	 * @param y
	 */
	public void addDotOn(int x,int y){
		addDotOn(x,y,DOT);
	}
	
	/** agrega la imagen de pacman en x,y
	 * @param x
	 * @param y
	 * @param imageFileName
	 */
	public void addPacmanOn(int x,int y,String imageFileName){
		addActorOn(pacman,x,y,imageFileName);
	}
	
	/**
	 * agrega a un actor en x,y
	 * @param actor
	 * @param x
	 * @param y
	 * @param imageFileName
	 */
	private void addActorOn(PCActor actor, int x,int y, String imageFileName){
		actor.setImageFileName(imageFileName);
		actor.moveTo(map.getBox(x,y));
	}
	
	/**
	 * agrega a blinky en x,y
	 * @param x
	 * @param y
	 * @param imageFileName
	 */
	public void addBlinkyOn(int x,int y, String imageFileName){
		Ghost ghost=new bGhost(pacman);
		addGhostOn(ghost,x,y,imageFileName);
	}
	
	/**
	 * agrega un dot en x,y
	 * @param x
	 * @param y
	 * @param imageFileName
	 */
	public void addDotOn(int x,int y, String imageFileName){
		PCdot dot=new PCdot(pacman);
		addDotOn(dot,x,y,imageFileName);
	}
	
	/**
	 * agrega a pinky en x,y
	 * @param x
	 * @param y
	 * @param imageFileName
	 */
	public void addPinkyOn(int x,int y, String imageFileName){
		Ghost ghost=new pcGhost(pacman);
		addGhostOn(ghost,x,y,imageFileName);
	}
	
	/**
	 * agrega a inky en x,y
	 * @param x
	 * @param y
	 * @param imageFileName
	 */
	public void addInkyOn(int x,int y, String imageFileName){
		Ghost ghost=new iGhost(pacman);
		addGhostOn(ghost,x,y,imageFileName);
	}
	
	/**
	 * agrega a clyde en x,y
	 * @param x
	 * @param y
	 * @param imageFileName
	 */
	public void addClydeOn(int x,int y, String imageFileName){
		Ghost ghost=new pcGhost(pacman);
		addGhostOn(ghost,x,y,imageFileName);
	}
	
	
	/**
	 * agrega un fantasma en x,y
	 * @param ghost
	 * @param x
	 * @param y
	 * @param imageFileName
	 */
	public void addGhostOn(Ghost ghost,int x,int y, String imageFileName){
		pacman.addObserver(ghost);
		addActorOn(ghost,x,y,imageFileName);
		(new Thread(ghost)).start();
	}
	
	/**
	 * agrega un dot en x,y
	 * @param dot
	 * @param x
	 * @param y
	 * @param imageFileName
	 */
	public void addDotOn(PCdot dot,int x,int y, String imageFileName){
		pacman.addObserver(dot);
		addActorOn(dot,x,y,imageFileName);
	}
	
	
	/**
	 * agrega una muralla de xo,yo a xf,yf
	 * @param xo
	 * @param yo
	 * @param xf
	 * @param yf
	 */
	public void addWall(int xo, int yo, int xf,int yf){
		for(int x=xo;x<=xf;x++){
			for(int y=yo;y<= yf;y++){
				addBlockOn(x, y);
			}			
		}
	}
	
	/**
	 * construye todo y abre el juego
	 */
	public void buildAndOpen(){
		map.loadBoxes();
		JFrame frame=map.openInWindow();
		frame.setFocusable(true);
		frame.setResizable(false);
		frame.setSize(map.getPreferredSize());
		frame.addKeyListener(pacman);
		map.updateUI();
	}
	
}
