package cc3002.pclike.pacman;

import static org.junit.Assert.assertTrue;
import cc3002.pclike.elements.Pacman;
import cc3002.pclike.elements.bGhost;



/**
 * @author ismailove
 * main para jugar
 */
public class MainClass {
	/**
	 * main
	 * @param args
	 * @throws InterruptedException
	 */
	public static void main(String[] args) throws InterruptedException {
		mapa3();
		
	}
	
	/**
	 * primer mapa entregado por enunciado
	 */
	public static void mapa1(){
		GameBuilder game=new GameBuilder(10,10,0);
		game.addWall(3,3, 3, 5);
		game.addPacmanOn(1,1);
		game.buildAndOpen();
		game.addBlinkyOn(10,10,GameBuilder.BLINKY);
		game.addInkyOn(1,10,GameBuilder.INKY);
		game.addPinkyOn(10,1,GameBuilder.PINKY);
		game.addClydeOn(5,5,GameBuilder.CLYDE);
	}
	
	/**
	 * segundo mapa, generado por uno mismo
	 */
	public static void mapa2(){
		GameBuilder game=new GameBuilder(21,21,12);
		
		game.addWall(6,3,6,4);
		game.addWall(4,4,5,4);
		game.addWall(7,4,8,4);
		game.addWall(5,8,5,13);
		game.addWall(6,3,6,4);
		game.addWall(4,17,8,17);
		game.addWall(6,18,6,19);
		
		game.addWall(9,8,10,8);
		game.addWall(9,13,10,13);
		game.addWall(10,8,10,13);
		
		game.addWall(14,4,19,4);
		game.addWall(17,4,17,6);
		game.addWall(14,6,15,6);
		game.addWall(15,7,15,8);
		game.addWall(16,8,19,8);
		game.addWall(19,6,19,7);
		
		game.addWall(14,10,14,18);
		game.addWall(19,10,19,18);
		game.addWall(16,10,18,10);
		game.addWall(16,14,18,14);
		game.addWall(15,12,17,12);
		game.addWall(15,16,17,16);
		game.addWall(17,17,17,18);
		
		game.addDotOn(5,3);
		game.addDotOn(5,18);
		game.addDotOn(16,5);
		game.addDotOn(16,7);
		game.addDotOn(18,8);
		game.addDotOn(18,7);
		game.addDotOn(15,11);
		game.addDotOn(15,13);
		game.addDotOn(15,15);
		game.addDotOn(18,11);
		game.addDotOn(18,13);
		game.addDotOn(18,13);
		
		game.addPacmanOn(11,10);
		game.buildAndOpen();
		game.addBlinkyOn(9,10,GameBuilder.BLINKY);
		game.addInkyOn(9,9,GameBuilder.INKY);
		game.addPinkyOn(9,11,GameBuilder.PINKY);
		game.addClydeOn(9,12,GameBuilder.CLYDE);
	}
	
	/**
	 * tercer mapa generado por uno mismo
	 */
	public static void mapa3(){
		GameBuilder game=new GameBuilder(21,21,10);
		
		game.addWall(2,8,2,9);
		game.addWall(4,8, 4, 9);
		game.addWall(2,12, 2, 13);
		game.addWall(4,12,4,13);
		
		game.addWall(6,9,7,9);
		game.addWall(9,9,10,9);
		game.addWall(8,9,8,10);
		
		game.addWall(6,11,6,12);
		game.addWall(7,12,9,12);
		game.addWall(10,11,10,12);
		
		game.addWall(12,4,12,5);
		game.addWall(12,7,12,8);
		game.addWall(13,4,15,4);
		game.addWall(13,8,15,8);
		game.addWall(15,5,15,7);
		
		game.addWall(12,12,12,13);
		game.addWall(12,15,12,16);
		game.addWall(13,12,15,12);
		game.addWall(13,16,15,16);
		game.addWall(15,13,15,15);
		
		game.addDotOn(7,10);
		game.addDotOn(7,11);
		game.addDotOn(9,10);
		game.addDotOn(9,11);
		game.addDotOn(14,5);
		game.addDotOn(14,6);
		game.addDotOn(14,7);
		game.addDotOn(14,13);
		game.addDotOn(14,14);
		game.addDotOn(14,15);
		
		game.addPacmanOn(5,10);
		game.buildAndOpen();
		game.addBlinkyOn(3,12,GameBuilder.BLINKY);
		game.addInkyOn(3,8,GameBuilder.INKY);
		game.addPinkyOn(3,9,GameBuilder.PINKY);
		game.addClydeOn(3,13,GameBuilder.CLYDE);
	}
	
}
