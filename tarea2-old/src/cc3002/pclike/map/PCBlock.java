package cc3002.pclike.map;

import java.awt.Color;


/**
 * @author ismailove
 * abstraccion de la muralla en el pacman
 */
public class PCBlock extends PCBox{

	/**
	 * serial version to jump eclipse warning
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * constructor del muro
	 */
	public PCBlock(){
		super();
		setOpaque(true);
		setBackground(Color.BLACK);
		revalidate();
	}

	/* (non-Javadoc)
	 * @see cc3002.pclike.map.PCBox#isBlock()
	 */
	@Override
	public boolean isBlock() {
		return true;
	}

}
