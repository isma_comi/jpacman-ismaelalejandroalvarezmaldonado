package cc3002.pclike.map;


import cc3002.pclike.elements.*;

/**
 * @author ismailove
 * caja vacia sin elementos el ella
 */
public class PCEmptyBox extends PCBox{
	
	/**
	 * indice i de la caja
	 */
	private int i;
	/**
	 * indice j de la caja
	 */
	private int j;
	
	/**
	 * constructor que setea los indices de las cajas
	 * @param i
	 * @param j
	 */
	public PCEmptyBox(int i, int j){
		this.i=i;
		this.j=j;
	}
	
	/**
	 * geter de i
	 * @return i
	 */
	public int geti(){
		return i;
	}
	
	/** geter de j
	 * @return j
	 */
	public int getj(){
		return j;
	}
	
	/**
	 * no tengo idea que es esto
	 */
	private static final long serialVersionUID = 1L;
	
	
	/* (non-Javadoc)
	 * @see cc3002.pclike.map.PCBox#moveUp(cc3002.pclike.elements.PCElement)
	 */
	public void moveUp(PCElement element){
		element.moveTo(topBox);
	}
	
	/* (non-Javadoc)
	 * @see cc3002.pclike.map.PCBox#moveDown(cc3002.pclike.elements.PCElement)
	 */
	public void moveDown(PCElement element){
		element.moveTo(downBox);
	}
	/* (non-Javadoc)
	 * @see cc3002.pclike.map.PCBox#moveLeft(cc3002.pclike.elements.PCElement)
	 */
	public void moveLeft(PCElement element){
		element.moveTo(leftBox);
	}
	
	/* (non-Javadoc)
	 * @see cc3002.pclike.map.PCBox#moveRight(cc3002.pclike.elements.PCElement)
	 */
	public void moveRight(PCElement element){
		element.moveTo(rightBox);
	}
	
}
