package cc3002.pclike.map;

import java.awt.*;
import java.util.ArrayList;
import java.util.LinkedList;

import javax.swing.*;

import cc3002.pclike.elements.*;

/**
 * @author ismailove
 * abstraccion de una celda
 */
public class PCBox extends JLabel{
	
	/**
	 * lista de las celdas
	 */
	private LinkedList<PCElement> elements;
	
	/**
	 * celda que esta arriba
	 */
	protected PCBox topBox;
	/**
	 * celda que esta abajo
	 */
	protected PCBox downBox;
	/**
	 * celda que esta a la izquerda
	 */
	protected PCBox leftBox;
	/**
	 * celda que esta a la derecha
	 */
	protected PCBox rightBox;
	
	/**
	 * constructor de la clase
	 */
	public PCBox(){
		elements = new LinkedList<PCElement>();
		elements.add(new PCNullElement());
		setPreferredSize(new Dimension(24,24));
		setMaximumSize(new Dimension(24,24));
		setMinimumSize(new Dimension(24,24));
		setBorder(BorderFactory.createLineBorder(Color.BLACK));
		setOpaque(true);
	}
	
	/**
	 * pregunta si es un bloque
	 * @return false no soy un bloque
	 */
	public boolean isBlock(){
		return false;
	}
	
	/**
	 * abstraccion de moverse hacia arriba
	 * @param element el elemento a mover
	 */
	public void moveUp(PCElement element){}
	/**
	 * abstraccion de moverse hacia abajo
	 * @param element el elemento a mover
	 */
	public void moveDown(PCElement element){}
	/**
	 * abstraccion de moverse hacia la izquerda
	 * @param element el elemento a mover
	 */
	public void moveLeft(PCElement element){}
	/**
	 * abstraccion de moverse hacia la derecha
	 * @param element el elemento a mover
	 */
	public void moveRight(PCElement element){}
	
	/**
	 * setea quienes son mis vecinbos
	 * @param _top el de arriba
	 * @param _down el de abajo
	 * @param _left el de la izquerda
	 * @param _right el de la derecha
	 */
	public void updateNeighbors(PCBox _top, PCBox _down,PCBox _left, PCBox _right){
		topBox = _top;
		downBox = _down;
		leftBox = _left;
		rightBox = _right;
	}
	
	/**
	 * agrega un elemento a la celda
	 * @param newElement el elemento a agregar
	 */
	public void add(PCElement newElement){
		elements.addFirst(newElement);
		updateBox();
	}
	
	/**
	 * quita un elemento de la celda
	 * @param element ele elemento a quitar
	 */
	public void remove(PCElement element){
		elements.remove(element);
		updateBox();
	}
	
	/**
	 * actualiza la caja
	 */
	public void updateBox(){
		setIcon(new ImageIcon(elements.get(0).fileImageName()));
		repaint();
	}
}
