package cc3002.pclike.map;

import java.awt.BorderLayout;
import java.awt.GridLayout;

import javax.swing.*;


/**
 * @author ismailove
 *
 */
public class PCMap extends JPanel{
	
	/**
	 * serial version to jump eclipse warning
	 */
	private static final long serialVersionUID = 1L;
	
	
	/**
	 * el reticulado de cajas
	 */
	private PCBox[][] box;
	/**
	 * primer indice de el maximo del mapa
	 */
	private int n;
	/**
	 * segundo indice del maximo de la caja
	 */
	private int m;
	
	/**
	 * construlle el mapa con todas sus cajas
	 * @param _n
	 * @param _m
	 */
	public PCMap(int _n,int _m){
		box = new PCBox[_n+2][_m+2];
		for(int i=0;i<_n+2;i++)
			for(int j=0;j<_m+2;j++)
				box[i][j]=new PCBlock();
		n = _n;
		m = _m;
		setLayout(new GridLayout(n,m));		
		fillMatrix();
		updateNeighbors();
	}
	
	/**
	 * setea la caja que esta en el i,j dandole _box
	 * @param i
	 * @param j
	 * @param _box
	 */
	public void setBox(int i,int j,PCBox _box){
		box[i][j]=_box;
		updateNeighbors();
		updateUI();
	}
	/**
	 * retorna la caja en el lugar i,j
	 * @param i
	 * @param j
	 * @return box[i][j]
	 */
	public PCBox getBox(int i,int j){
		return box[i][j];
	}
	
	
	/**
	 * genera la ventana
	 * @return frame el frame
	 */
	public JFrame openInWindow(){
		JFrame frame=new JFrame();
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setLayout(new BorderLayout());
		frame.add(this, BorderLayout.CENTER);
		frame.setVisible(true);
		updateUI();
		return frame;
	}
	
	/**
	 * llena el mapa con cajas vacias
	 */
	private void fillMatrix(){
		for(int i=1; i<=n;i++){
			for(int j=1;j<=m;j++){
				box[i][j]=new PCEmptyBox(i,j);
			}
		}
	}
	
	/**
	 * agrega las cajas
	 */
	public void loadBoxes(){
		for(int i=1; i<=n;i++){
			for(int j=1;j<=m;j++){
				add(box[i][j]);
			}
		}
	}
	
	/**
	 * le avisa a las cajas cuales son sus vecinos
	 */
	private void updateNeighbors(){
		for(int i=1; i<=n;i++){
			for(int j=1;j<=m;j++){
				box[i][j]
					.updateNeighbors(
							box[i-1][j],
							box[i+1][j],
							box[i][j-1],
							box[i][j+1]);
				box[i][j].updateBox();
			}
		}
	}
}
