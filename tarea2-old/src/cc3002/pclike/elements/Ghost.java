package cc3002.pclike.elements;

import java.util.Observable;
import java.util.Observer;

import javax.swing.JOptionPane;

import cc3002.pclike.map.PCBox;
import cc3002.pclike.map.PCEmptyBox;


/**
 * @author ismailove
 * clase abstracta que abstrae la idea de fantasma del pacman
 */
public abstract class Ghost extends PCActor implements Runnable, Observer{

	/**
	 * boolean que indica si el fantasma esta activo
	 */
	private boolean active; 
	/**
	 * int que guarda la informacion de con que velosidad se mueve el fantasma
	 */
	protected int speed; // milliseconds
	
	/**
	 * constructor que encapsula generalidades de los constructores de los fantasmas
	 * @param _pacman
	 */
	public Ghost(Pacman _pacman){
		active = true;
		pacman = _pacman;
	}
	
	/* (non-Javadoc)
	 * @see cc3002.pclike.elements.PCActor#moveTo(cc3002.pclike.map.PCBox)
	 */
	public void moveTo(PCBox box){
		super.moveTo(box);
		checkPosition();
	}
	
	/**
	 * funcion que encapsula la idea de moverse aleatoriamente
	 */
	public void moveToNextCell(){
		checkPosition();
		double random=Math.random();
		if(random<0.25) moveDown();
		else if(random <0.5) moveLeft();
		else if(random <0.75) moveRight();
		else moveUp();
	}
	
	/**
	 * funcion que imposibilita al fantasma de moverse
	 */
	public void disable(){
		active=false;
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Runnable#run()
	 */
	public void run(){
		while(active){
			try{Thread.sleep(speed);} catch(Exception e){}
			this.moveToNextCell();
		}
	}

	/* (non-Javadoc)
	 * @see java.util.Observer#update(java.util.Observable, java.lang.Object)
	 */
	@Override
	public void update(Observable obs, Object args) {
		checkPosition();
	}
	
	/**
	 * funcion que verifica si el fantasma alcanzo al pacman
	 */
	private void checkPosition(){
		if(pacman.currentBox == currentBox){
			JOptionPane.showMessageDialog(null,"Game Over");
			System.exit(0);
		}
	}
	
	/**
	 * funcion que retorna la direccion en la que esta el pacman
	 * @return direccion es la direccion en la que esta el pacman
	 */
	protected direccion direc(){
		PCEmptyBox tub = (PCEmptyBox) pacman.currentBox;
		PCEmptyBox yob = (PCEmptyBox) currentBox;
		int difi = tub.geti()-yob.geti();
		int difj = tub.getj()-yob.getj();
		if(Math.abs(difi)<Math.abs(difj)){
			if(difj<0) return new izquerda();
			else return new derecha();
		}
		else{
			if(difi<0) return new arriba();
			else return new abajo();
		}
	}
}
