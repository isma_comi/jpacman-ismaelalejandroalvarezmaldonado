package cc3002.pclike.elements;

import java.util.Observable;

import cc3002.pclike.map.PCBox;

/**
 * @author ismailove
 * clase que abstrae la idea de los elementos en el juego, como observables
 */
public abstract class PCElement extends Observable{
	/**
	 * string que almacena la direccion de la imagen del objeto
	 */
	protected String fileImageName;

	/**
	 * constructor del elemento
	 */
	public PCElement(){
		fileImageName="";
	}
	
	
	/**
	 * geter de la direccion del elemento
	 * @return
	 */
	public String fileImageName(){
		return fileImageName;
	}
	
	/**
	 * seter de la imagen del elemento
	 * @param name la direccion de la imagen del elemento
	 */
	public void setImageFileName(String name){
		fileImageName=name;
	}
	
	/**
	 * idea de mover elemento hacia una casilla
	 * @param box la casilla a la que hay que moverse
	 */
	public void moveTo(PCBox box){
		
	}

}
