package cc3002.pclike.elements;

/**
 * @author ismailove
 * clase de los ghost mas tontos que se mueven aleatoriamente
 */
public class pcGhost extends Ghost{

	/**
	 * constructor de los fantasmas tontos
	 * @param _pacman referencia a la unica instancia del pacman
	 */
	public pcGhost(Pacman _pacman) {
		super(_pacman);
		speed = 1000;
	}

}
