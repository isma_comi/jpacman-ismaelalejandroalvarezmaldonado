package cc3002.pclike.elements;

import cc3002.pclike.map.PCBlock;
import cc3002.pclike.map.PCBox;


/**
 * @author ismailove
 * abstraccion de los elementos que actuan en el juego
 */
public abstract class PCActor extends PCElement{
	
	/**
	 * referencia a pacman para notificar y realizar acciones
	 */
	protected Pacman pacman;
	/**
	 * referencia a la casilla en la que se encuentra
	 */
	protected PCBox currentBox;
	
	/**
	 * constructor que genera la casilla en la que esta el actor
	 */
	public PCActor(){
		currentBox=new PCBlock();
	}

	
	
	/* (non-Javadoc)
	 * @see cc3002.pclike.elements.PCElement#moveTo(cc3002.pclike.map.PCBox)
	 */
	public void moveTo(PCBox box){
		if(!box.isBlock()){
			currentBox.remove(this);
			currentBox=box;
			currentBox.add(this);
		}
	}
	

	/**
	 * 
	 */
	public void moveUp() { currentBox.moveUp(this);}
	/**
	 * 
	 */
	public void moveDown() { currentBox.moveDown(this); }
	/**
	 * 
	 */
	public void moveLeft() { currentBox.moveLeft(this); }
	/**
	 * 
	 */
	public void moveRight() { currentBox.moveRight(this); }

	

}
