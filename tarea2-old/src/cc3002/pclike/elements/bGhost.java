package cc3002.pclike.elements;

import java.util.Observable;

/**
 * @author ismailove
 * clase del fantasma blinky
 */
public class bGhost extends Ghost{

	/**
	 * constructor que le da la referencia del pacman que tiene que perseguir
	 * @param _pacman es el pacman
	 */
	public bGhost(Pacman _pacman) {
		super(_pacman);
		speed = 500;
	}
	
	
	/* (non-Javadoc)
	 * @see cc3002.pclike.elements.Ghost#moveToNextCell()
	 */
	public void moveToNextCell(){
		muevete(direc());
	}
	
	/** geter del speed
	 * @return speed es la velosidad del fantasma
	 */
	public int getSpeed(){
		return speed;
	}
	
	/**
	 * funcion que encapsula el pomportamiento de blinky para seguir a pacman
	 * @param dir
	 */
	private void muevete(direccion dir){
		if(dir instanceof abajo) moveDown();
		else if(dir instanceof izquerda) moveLeft();
		else if(dir instanceof derecha) moveRight();
		else moveUp();
	}
	
	/* (non-Javadoc)
	 * @see cc3002.pclike.elements.Ghost#update(java.util.Observable, java.lang.Object)
	 */
	@Override
	public void update(Observable obs, Object args) {
		super.update(obs, args);
		if(pacman.getComido()==8){
			this.speed = 350;
		}
	}

}
