package cc3002.pclike.elements;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.JOptionPane;

import cc3002.pclike.map.PCBox;

/**
 * @author ismailove
 * clase del pacman
 */
public class Pacman extends PCActor implements KeyListener{
	
	/**
	 * unica instancia del pacman
	 */
	private static Pacman INSTANCE = new Pacman();
	/**
	 * numero del dots que ha comido
	 */
	private int comido;
	/**
	 * numero de dots que tiene que comer para ganar
	 */
	private int maximo;
	
	/**
	 * constructor privado para solo tener una unica instancia
	 */
	private Pacman() {}
	
	/**
	 * el geter de la unica instancia
	 * @return INSTANCE es la unica instancia de pacman
	 */
	public static Pacman getInstance() {
        return INSTANCE;
    }
	
	/**
	 * geter del maximo
	 * @return maximo
	 */
	public int getMax(){
		return maximo;
	}
	
	/**
	 * setea los valores de cuanto ha comido y cuanto tiene que comer
	 * @param max es el numero de dots a comer
	 */
	public void setallstart(int max){
		comido=0;
		maximo=max;
	}
	
	/**
	 * geter de cuanto ha comido
	 * @return comido cuanto ha comido
	 */
	public int getComido(){
		return comido;
	}
	
	/* (non-Javadoc)
	 * @see cc3002.pclike.elements.PCActor#moveTo(cc3002.pclike.map.PCBox)
	 */
	public void moveTo(PCBox box){
		super.moveTo(box);
		setChanged();
		notifyObservers();
	}
	
	/* (non-Javadoc)
	 * @see java.awt.event.KeyListener#keyPressed(java.awt.event.KeyEvent)
	 */
	@Override
	public void keyPressed(KeyEvent event) {
		switch(event.getKeyCode()){
		case KeyEvent.VK_UP : moveUp(); break;
		case KeyEvent.VK_DOWN: moveDown();break;
		case KeyEvent.VK_LEFT: moveLeft(); break;
		case KeyEvent.VK_RIGHT: moveRight();break;
		}
	}
	/* (non-Javadoc)
	 * @see java.awt.event.KeyListener#keyReleased(java.awt.event.KeyEvent)
	 */
	@Override
	public void keyReleased(KeyEvent event) {
		
	}

	/* (non-Javadoc)
	 * @see java.awt.event.KeyListener#keyTyped(java.awt.event.KeyEvent)
	 */
	@Override
	public void keyTyped(KeyEvent event) {
	
	}

	/**
	 * funcion que realiza la accion de come, avisa si se ha comido 8 y si se ha comido todos gana
	 */
	public void come() {
		comido = comido +1;
		if(comido==8){
			notifyObservers();
		}
		if(comido==maximo){
			JOptionPane.showMessageDialog(null,"Congratulations! You Won!") ;
			System.exit(0) ;

		}
	}
}
