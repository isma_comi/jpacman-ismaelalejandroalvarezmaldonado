package cc3002.pclike.elements;

import java.util.Observable;
import java.util.Observer;

/**
 * @author ismailove
 * clase de los dots que tiene que comer el pacman
 */
public class PCdot extends PCActor implements Observer{

	/**
	 * booleano que indica si ha sido o no comido el dot
	 */
	private boolean nocomido;
	
	/**
	 * constructor que tiene la referencia al pacman
	 * @param _pacman el unico pacman que hay
	 */
	public PCdot(Pacman _pacman){
		pacman = _pacman;
		nocomido = true;
	}
	
	/* (non-Javadoc)
	 * @see java.util.Observer#update(java.util.Observable, java.lang.Object)
	 */
	@Override
	public void update(Observable obs, Object args) {
		checkPosition();
	}
	
	/**
	 * funcion que verifica si ha sido comido, si no estaba comido se pone como comido y le dice al pacman que se comio un dot
	 */
	private void checkPosition(){
		if(nocomido){
			if(pacman.currentBox == currentBox){
				pacman.come();
				nocomido=false;
				pacman.currentBox.remove(this);
			}
		}
	}

	
	
}
