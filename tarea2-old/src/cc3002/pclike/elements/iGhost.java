package cc3002.pclike.elements;

/**
 * @author ismailove
 * clase del fantasma inky
 */
public class iGhost extends Ghost{

	/**
	 * constructor del fantasma que le da la referencia del pacman
	 * @param _pacman es el pacman
	 */
	public iGhost(Pacman _pacman) {
		super(_pacman);
		speed = 1200;
	}
	
	/* (non-Javadoc)
	 * @see cc3002.pclike.elements.Ghost#moveToNextCell()
	 */
	public void moveToNextCell(){
		double random=Math.random();
		if(random<=0.5) super.moveToNextCell();
		else{
			muevete(direc());
		}
	}
	
	/**
	 * funcion que encapsula el comportamiento del movimiento de inky deacuerdo a la ubicacion del pacman
	 * @param dir
	 */
	private void muevete(direccion dir){
		if(dir instanceof arriba) moveDown();
		else if(dir instanceof derecha) moveLeft();
		else if(dir instanceof izquerda) moveRight();
		else moveUp();
	}

}
